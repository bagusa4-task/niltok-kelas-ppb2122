package com.niltok.temancerpen

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.os.StrictMode
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.amulyakhare.textdrawable.TextDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.niltok.temancerpen.data.Cerpen
import com.niltok.temancerpen.databinding.ActivityDetailBinding
import com.niltok.temancerpen.db.CerpenHelper
import com.niltok.temancerpen.db.DatabaseContract
import com.niltok.temancerpen.helper.EXTRA_CERPEN
import com.niltok.temancerpen.helper.REQUEST_UPDATE
import com.niltok.temancerpen.helper.RESULT_ADD
import com.niltok.temancerpen.ui.CerpenAddUpdateActivity
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding
    private lateinit var dialog: BottomSheetDialog
    private val myData by getParcelableExtra<Cerpen>(DetailActivity.EXTRA_MYDATA)
    private lateinit var cerpenHelper: CerpenHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Open Sqlite Database
        cerpenHelper = CerpenHelper.getInstance(applicationContext)
        cerpenHelper.open()

        setSupportActionBar(findViewById(R.id.toolbar))
//        binding.toolbarLayout.title = title
        binding.toolbarLayout.title = myData?.title.toString()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // no restriction to share data on other apps
        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        binding.fab.setOnClickListener { view ->
            fabBottomDialog()
//            Snackbar.make(dialog.window?.decorView ?: view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
        }

//        Set Content
        Glide.with(this)
            .load(myData?.cover.toString())
            .placeholder(TextDrawable.builder().buildRect(myData?.title?.take(2), ContextCompat.getColor(this, R.color.bright_sky_blue_3)))
            .apply(RequestOptions().override(700, 700))
            .into(binding.ivDetailPhoto)
        val textContent = "${myData?.content.toString()} \n\n" + "${myData?.author.toString()} - " +
                "${myData?.category.toString()} - " +
                "${myData?.detail.toString()}"
        binding.contentScrolling.tvDetailContent.text = textContent
    //HtmlCompat.fromHtml(textContent, HtmlCompat.FROM_HTML_MODE_LEGACY)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun fabBottomDialog() {
        val view: View = layoutInflater.inflate(R.layout.bottom_dialog_detail, null)
        dialog = BottomSheetDialog(this)

        view.findViewById<Button>(R.id.button_edit).setOnClickListener {
            val intent = Intent(this, CerpenAddUpdateActivity::class.java)
            intent.putExtra(EXTRA_CERPEN, myData)
            this.startActivityForResult(intent, REQUEST_UPDATE)
        }

        view.findViewById<Button>(R.id.button_share).setOnClickListener {
            val intent: Intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "image/*"
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            val bmp = binding.ivDetailPhoto.drawable.toBitmap(700, 700)
            var bmpUri: Uri? = null
            try {
                val file = File(this.externalCacheDir, System.currentTimeMillis().toString() + ".jpg")
                val out = FileOutputStream(file)
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.close()
                bmpUri = Uri.fromFile(file)

            } catch (e: IOException) {
                e.printStackTrace()
            }
            intent.putExtra(Intent.EXTRA_STREAM, bmpUri)

            val textContent = "${myData?.title.toString()} \n\n" +
                    "${myData?.content.toString()} \n\n" +
                    "${myData?.author.toString()} - " +
                    "${myData?.category.toString()} - " +
                    "${myData?.detail.toString()}"
            intent.putExtra(Intent.EXTRA_TEXT, textContent)

            startActivity(Intent.createChooser(intent, "Share Cerpen."))
        }

        // Insert Data to Likes Sqlite
        view.findViewById<Button>(R.id.button_favorite).setOnClickListener {
            val intent = Intent()
            val values = ContentValues()
            values.put(DatabaseContract.CerpenColumns._ID, myData?.id)
            values.put(DatabaseContract.CerpenColumns.COVER, myData?.cover)
            values.put(DatabaseContract.CerpenColumns.TITLE, myData?.title)
            values.put(DatabaseContract.CerpenColumns.CONTENT, myData?.content)
            values.put(DatabaseContract.CerpenColumns.AUTHOR, myData?.author)
            values.put(DatabaseContract.CerpenColumns.CATEGORY, myData?.category)
            values.put(DatabaseContract.CerpenColumns.DETAIL, myData?.detail)
            val result = cerpenHelper.insert(values)
            if (result > 0) {
                myData?.id = result.toString()
                setResult(RESULT_ADD, intent)
                Toast.makeText(this@DetailActivity, "Sukses menambah data",
                    Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this@DetailActivity, "Gagal menambah data",
                    Toast.LENGTH_SHORT).show()
            }
//            Toast.makeText(dialog.window?.decorView?.context ?: it.context, "How", Toast.LENGTH_LONG)
        }

        dialog.setContentView(view)
        dialog.show()
    }

    companion object {
        const val EXTRA_MYDATA = "extra_mydata"
    }
    inline fun <reified T : Parcelable> Activity.getParcelableExtra(key: String) = lazy {
        intent.getParcelableExtra<T>(key)
    }
}