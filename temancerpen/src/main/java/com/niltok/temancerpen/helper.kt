package com.niltok.temancerpen

import android.database.Cursor
import com.niltok.temancerpen.data.Cerpen
import com.niltok.temancerpen.db.DatabaseContract
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

object helper {
    var categoryList = arrayOf(
        "Motivasi",
        "Persahabatan",
        "Percintaan",
        "Keluarga",
        "Musik",
        "Film"
    )
    const val EXTRA_CERPEN = "extra_cerpen"
    const val EXTRA_POSITION = "extra_position"
    const val REQUEST_ADD = 100
    const val RESULT_ADD = 101
    const val REQUEST_UPDATE = 200
    const val RESULT_UPDATE = 201
    const val RESULT_DELETE = 301
    const val ALERT_DIALOG_CLOSE = 10
    const val ALERT_DIALOG_DELETE = 20

    fun mapCursorToArrayList(notesCursor: Cursor?): ArrayList<Cerpen> {
        val CerpensList = ArrayList<Cerpen>()
        notesCursor?.apply {
            while (moveToNext()) {
                val id =
                    getString(getColumnIndexOrThrow(DatabaseContract.CerpenColumns._ID))
                val cover =
                    getString(getColumnIndexOrThrow(DatabaseContract.CerpenColumns.COVER))
                val title =
                    getString(getColumnIndexOrThrow(DatabaseContract.CerpenColumns.TITLE))
                val content =
                    getString(getColumnIndexOrThrow(DatabaseContract.CerpenColumns.CONTENT))
                val author =
                    getString(getColumnIndexOrThrow(DatabaseContract.CerpenColumns.AUTHOR))
                val category =
                    getString(getColumnIndexOrThrow(DatabaseContract.CerpenColumns.CATEGORY))
                val detail =
                    getString(getColumnIndexOrThrow(DatabaseContract.CerpenColumns.DETAIL))
                CerpensList.add(Cerpen(id, cover, title, content, author, category, detail))
            }
        }
        return CerpensList
    }
    fun getCurrentDate(): String {
        val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault())
        val date = Date()
        return dateFormat.format(date)
    }
}