package com.niltok.temancerpen.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.niltok.temancerpen.db.DatabaseContract.CerpenColumns.Companion.TABLE_CERPEN

internal class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME,
    null, DATABASE_VERSION) {
    companion object {
        private const val DATABASE_NAME = "dbquoteapp"
        private const val DATABASE_VERSION = 5
        private const val SQL_CREATE_TABLE_QUOTE =
            "CREATE TABLE $TABLE_CERPEN" +
            " (${DatabaseContract.CerpenColumns._ID} TEXT PRIMARY KEY," +
            " ${DatabaseContract.CerpenColumns.COVER} TEXT NOT NULL," +
            " ${DatabaseContract.CerpenColumns.TITLE} TEXT NOT NULL," +
            " ${DatabaseContract.CerpenColumns.CONTENT} TEXT NOT NULL," +
            " ${DatabaseContract.CerpenColumns.AUTHOR} TEXT NOT NULL," +
            " ${DatabaseContract.CerpenColumns.CATEGORY} TEXT NOT NULL," +
            " ${DatabaseContract.CerpenColumns.DETAIL} TEXT NOT NULL)"
    }
    override fun onCreate(db: SQLiteDatabase?) {
        if (db != null) {
            db.execSQL(SQL_CREATE_TABLE_QUOTE)
        }
    }
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        if (db != null) {
            db.execSQL("DROP TABLE IF EXISTS $TABLE_CERPEN")
        }
        onCreate(db)
    }
}