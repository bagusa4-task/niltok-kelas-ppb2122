package com.niltok.temancerpen.db

import android.provider.BaseColumns

class DatabaseContract {
    internal class CerpenColumns : BaseColumns {
        companion object {
            const val TABLE_CERPEN = "cerpen"
            const val _ID = "_id"
            const val COVER = "cover"
            const val TITLE = "title"
            const val CONTENT = "content"
            const val AUTHOR = "author"
            const val CATEGORY = "category"
            const val DETAIL = "detail"
        }
    }
}