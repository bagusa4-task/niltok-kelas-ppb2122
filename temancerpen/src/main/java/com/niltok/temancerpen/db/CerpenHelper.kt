package com.niltok.temancerpen.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import com.niltok.temancerpen.db.DatabaseContract.CerpenColumns.Companion.TABLE_CERPEN
import com.niltok.temancerpen.db.DatabaseContract.CerpenColumns.Companion._ID

class CerpenHelper(context: Context) {
    companion object {
        private lateinit var dataBaseHelper: DatabaseHelper
        private var INSTANCE: CerpenHelper? = null
        private lateinit var database: SQLiteDatabase
        fun getInstance(context: Context): CerpenHelper =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: CerpenHelper(context)
            }
    }
    init {
        dataBaseHelper = DatabaseHelper(context)
    }
    @Throws(SQLException::class)
    fun open() {
        database = dataBaseHelper.writableDatabase
    }
    fun close() {
        dataBaseHelper.close()
        if (database.isOpen)
            database.close()
    }
    fun queryAll(): Cursor {
        return database.query(
            TABLE_CERPEN,
            null,
            null,
            null,
            null,
            null,
            "$_ID ASC")
    }
    fun queryById(id: String): Cursor {
        return database.query(
            TABLE_CERPEN,
            null,
            "$_ID = ?",
            arrayOf(id),
            null,
            null,
            null,
            null)
    }
    fun insert(values: ContentValues?): Long {
        return database.insert(
            TABLE_CERPEN, null, values)
    }
    fun update(id: String, values: ContentValues?): Int {
        return database.update(
            TABLE_CERPEN, values, "$_ID = ?", arrayOf(id))
    }
    fun deleteById(id: String): Int {
        return database.delete(
            TABLE_CERPEN, "$_ID = '$id'", null)
    }
}