package com.niltok.temancerpen.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Cerpen (
    var id: String? = null,
    var cover: String? = null,
    var title: String? = null,
    var content: String? = null,
    var author: String? = null,
    var category: String? = null,
    var detail: String? = null,
): Parcelable