package com.niltok.temancerpen.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.niltok.temancerpen.MainActivity
import com.niltok.temancerpen.R
import com.niltok.temancerpen.data.Cerpen
import com.niltok.temancerpen.databinding.ActivityCerpenAddUpdateBinding
import com.niltok.temancerpen.helper.ALERT_DIALOG_CLOSE
import com.niltok.temancerpen.helper.ALERT_DIALOG_DELETE
import com.niltok.temancerpen.helper.EXTRA_CERPEN
import com.niltok.temancerpen.helper.EXTRA_POSITION
import com.niltok.temancerpen.helper.RESULT_ADD
import com.niltok.temancerpen.helper.RESULT_DELETE
import com.niltok.temancerpen.helper.RESULT_UPDATE

class CerpenAddUpdateActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityCerpenAddUpdateBinding

    private lateinit var auth: FirebaseAuth
    private lateinit var firestore: FirebaseFirestore

    private var isEdit = false
    private var cerpen: Cerpen? = null
    private var position: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCerpenAddUpdateBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firestore = Firebase.firestore
        auth = Firebase.auth

        cerpen = intent.getParcelableExtra(EXTRA_CERPEN)
        if (cerpen != null) {
            position = intent.getIntExtra(EXTRA_POSITION, 0)
            isEdit = true
        } else {
            cerpen = Cerpen()
        }

        val actionBarTitle: String
        val btnTitle: String

        if (isEdit) {
            actionBarTitle = "Ubah"
            btnTitle = "Update"
            cerpen?.let {
                binding.edtTitle.setText(it.title)
                binding.edtCover.setText(it.cover)
                binding.edtContent.setText(it.content)
                binding.edtAuthor.setText(it.author)
                binding.edtCategory.setText(it.category)
                binding.edtDetail.setText(it.detail)

            }!!
        } else {
            actionBarTitle = "Tambah"
            btnTitle = "Simpan"
        }

        supportActionBar?.title = actionBarTitle
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.btnSubmit.text = btnTitle
        binding.btnSubmit.setOnClickListener(this)
    }

    override fun onBackPressed() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btn_submit) {
            val title = binding.edtTitle.text.toString().trim()
            val cover = binding.edtCover.text.toString().trim()
            val content = binding.edtContent.text.toString().trim()
            val author = binding.edtAuthor.text.toString().trim()
            val category = binding.edtCategory.text.toString().trim()
            val detail = binding.edtDetail.text.toString().trim()

            val cantBlank = "Field can not be blank"
            if (title.isEmpty()) {
                binding.edtTitle.error = cantBlank
                return
            }
            if (content.isEmpty()) {
                binding.edtContent.error = cantBlank
                return
            }
            if (author.isEmpty()) {
                binding.edtAuthor.error = cantBlank
                return
            }
            if (category.isEmpty()) {
                binding.edtCategory.error = cantBlank
                return
            }

            val currentUser = auth.currentUser
            val user = hashMapOf(
                "uid" to currentUser?.uid,
                "title" to title,
                "cover" to cover,
                "content" to content,
                "author" to author,
                "category" to category,
                "detail" to detail,
            )

            if (isEdit) {
                firestore.collection("cerpens").document(cerpen?.id.toString())
                    .set(user)
                    .addOnSuccessListener {
                        setResult(RESULT_UPDATE, intent)
                        finish()
                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(this@CerpenAddUpdateActivity, "Gagal mengupdate data",
                            Toast.LENGTH_SHORT).show()
                    }
            } else {
                firestore.collection("cerpens")
                .add(user)
                .addOnSuccessListener { documentReference ->
                    Toast.makeText(this@CerpenAddUpdateActivity,
                        "Document Snapshot added with ID: ${documentReference.id}",
                        Toast.LENGTH_SHORT).show()
                    setResult(RESULT_ADD, intent)
                    finish()
                }
                .addOnFailureListener { e ->
                    Toast.makeText(this@CerpenAddUpdateActivity,
                        "Error adding document", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (isEdit) {
            menuInflater.inflate(R.menu.menu_detail, menu)
        }
        return super.onCreateOptionsMenu(menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_delete -> showAlertDialog(ALERT_DIALOG_DELETE)
            android.R.id.home -> showAlertDialog(ALERT_DIALOG_CLOSE)
        }
        return super.onOptionsItemSelected(item)
        return true
    }

    private fun showAlertDialog(type: Int) {
        val isDialogClose = type == ALERT_DIALOG_CLOSE
        val dialogTitle: String
        val dialogMessage: String
        if (isDialogClose) {
            dialogTitle = "Batal"
            dialogMessage = "Apakah anda ingin membatalkan perubahan pada form?"
        } else {
            dialogMessage = "Apakah anda yakin ingin menghapus item ini?"
            dialogTitle = "Hapus Quote"
        }
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle(dialogTitle)
        alertDialogBuilder
            .setMessage(dialogMessage)
            .setCancelable(false)
            .setPositiveButton("Ya") { _, _ ->
                if (isDialogClose) {
                    finish()
                } else {
                    firestore.collection("cerpens").document(cerpen?.id.toString())
                        .delete()
                        .addOnSuccessListener {
                            Log.d("delete",
                                "DocumentSnapshot successfully deleted!"+cerpen?.id.toString())
                                val intent = Intent()
                            intent.putExtra(EXTRA_POSITION, position)
                            setResult(RESULT_DELETE, intent)
                            finish()
                        }
                        .addOnFailureListener { e ->
                            Log.w("a", "Error deleting document", e)
                            Toast.makeText(this@CerpenAddUpdateActivity,
                                "Gagal menghapus data",
                                Toast.LENGTH_SHORT).show()
                        }
                }
            }
            .setNegativeButton("Tidak") { dialog, _ -> dialog.cancel() }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}