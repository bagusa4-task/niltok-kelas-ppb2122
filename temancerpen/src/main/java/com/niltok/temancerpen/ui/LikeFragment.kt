package com.niltok.temancerpen.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.niltok.temancerpen.adapter.CardViewCerpenDataAdapter
import com.niltok.temancerpen.data.Cerpen
import com.niltok.temancerpen.databinding.FragmentLikeBinding
import com.niltok.temancerpen.db.CerpenHelper
import com.niltok.temancerpen.helper.mapCursorToArrayList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LikeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LikeFragment : Fragment() {
//    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null

    private var _binding: FragmentLikeBinding? = null
    private val binding get() = _binding!!

    private lateinit var cerpenHelper: CerpenHelper
    private lateinit var adapter: CardViewCerpenDataAdapter
    private val EXTRA_STATE = "EXTRA_STATE"

    private var listCerpens = ArrayList<Cerpen>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }

        cerpenHelper = CerpenHelper.getInstance(requireContext())
        cerpenHelper.open()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_like, container, false)
        _binding = FragmentLikeBinding.inflate(inflater, container, false)

        if (savedInstanceState == null) {
            loadQuotes()
        } else {
            val list = savedInstanceState.getParcelableArrayList<Cerpen>(EXTRA_STATE)
            if (list != null) {
                listCerpens = list
            }
        }

        binding.rvLikeCerpenData.setHasFixedSize(true)
        binding.rvLikeCerpenData.layoutManager = LinearLayoutManager(container?.context)
        adapter = CardViewCerpenDataAdapter(container!!.context)
        adapter.listCerpens = listCerpens
        binding.rvLikeCerpenData.adapter = adapter

        return binding.root
    }

    private fun loadQuotes() {
//        GlobalScope.launch(Dispatchers.Main) {
            binding.progressbar.visibility = View.VISIBLE
            val cursor = cerpenHelper.queryAll()
            var cerpens = mapCursorToArrayList(cursor)
            binding.progressbar.visibility = View.INVISIBLE
            if (cerpens.size > 0) {
                listCerpens = cerpens
            } else {
                listCerpens = ArrayList()
                showSnackbarMessage("Tidak ada data saat ini")
            }
//        }
    }

    private fun showSnackbarMessage(message: String) {
        Snackbar.make(binding.rvLikeCerpenData, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(EXTRA_STATE, listCerpens)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LikeFragment.
         */
//        // TODO: Rename and change types and number of parameters
//        @JvmStatic
//        fun newInstance(param1: String, param2: String) =
//            LikeFragment().apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
//            }
    }
}