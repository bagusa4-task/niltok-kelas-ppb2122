package com.niltok.temancerpen.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.niltok.temancerpen.R
import com.niltok.temancerpen.adapter.CardViewCerpenDataAdapter
import com.niltok.temancerpen.data.Cerpen
import com.niltok.temancerpen.databinding.FragmentLikeBinding
import com.niltok.temancerpen.databinding.FragmentRecyclerViewCerpenBinding
import com.niltok.temancerpen.helper.REQUEST_ADD
import com.niltok.temancerpen.helper.REQUEST_UPDATE
import com.niltok.temancerpen.helper.RESULT_ADD
import com.niltok.temancerpen.helper.RESULT_DELETE
import com.niltok.temancerpen.helper.RESULT_UPDATE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RecyclerViewFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RecyclerViewFragment : Fragment() {
    private var _binding: FragmentRecyclerViewCerpenBinding? = null
    private val binding get() = _binding!!

    private lateinit var rv_mydata: RecyclerView
    private lateinit var listCerpenss: ArrayList<Cerpen>
    private lateinit var cardViewMyDataAdapter: CardViewCerpenDataAdapter
    private lateinit var myContext: Context

    private lateinit var auth: FirebaseAuth
    private lateinit var firestore: FirebaseFirestore

//    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }

        firestore = Firebase.firestore
        auth = Firebase.auth

        loadCerpens()
        listCerpenss = ArrayList<Cerpen>()
//        list.addAll(getListMyDatas())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRecyclerViewCerpenBinding.inflate(inflater, container, false)
        val view: View = binding.root
        myContext = requireContext()

        rv_mydata = binding.rvCerpens
        rv_mydata.setHasFixedSize(true)
        showRecyclerCardView(view)

        return view
    }

    private fun showRecyclerCardView(view: View) {
        rv_mydata.layoutManager = LinearLayoutManager(view.context)
        cardViewMyDataAdapter  = CardViewCerpenDataAdapter(view.context)
        rv_mydata.adapter = cardViewMyDataAdapter
    }

    fun getListMyDatas(): ArrayList<Cerpen> {
        val dataId = resources.getStringArray(R.array.data_id)
        val dataCover = resources.getStringArray(R.array.data_cover)
        val dataTitle = resources.getStringArray(R.array.data_title)
        val dataContent = resources.getStringArray(R.array.data_content)
        val dataAuthor = resources.getStringArray(R.array.data_author)
        val dataCategory = resources.getStringArray(R.array.data_category)
        val dataDetail = resources.getStringArray(R.array.data_detail)

        val listMyData = ArrayList<Cerpen>()
        for (position in dataTitle.indices) {
            val myData = Cerpen(
                dataId[position],
                dataCover[position],
                dataTitle[position],
                dataContent[position],
                dataAuthor[position],
                dataCategory[position],
                dataDetail[position],
            )
            listMyData.add(myData)
        }

        return listMyData
    }

    private fun loadCerpens() {
        GlobalScope.launch(Dispatchers.Main) {
            binding.progressbar.visibility = View.VISIBLE
            val cerpensList = ArrayList<Cerpen>()
            val currentUser = auth.currentUser
            firestore.collection("cerpens")
//                .whereEqualTo("uid", currentUser?.uid)
                .get()
                .addOnSuccessListener { result ->
                    binding.progressbar.visibility = View.INVISIBLE
                    for (document in result) {
                        val id = document.id
                        val cover = document.get("cover").toString()
                        val title = document.get("title").toString()
                        val content = document.get("content").toString()
                        val author = document.get("author").toString()
                        val category = document.get("category").toString()
                        val detail = document.get("detail").toString()
                        cerpensList.add(Cerpen(id, cover, title, content, author, category, detail))
                    }
                    if (cerpensList.size > 0) {
                        listCerpenss = cerpensList
                        cardViewMyDataAdapter.listCerpens = listCerpenss
                        binding.rvCerpens.adapter = cardViewMyDataAdapter
                    } else {
                        listCerpenss.clear()
                        binding.rvCerpens?.adapter?.notifyDataSetChanged()
                        showSnackbarMessage("Tidak ada data saat ini")
                    }
                }
                .addOnFailureListener { exception ->
                    binding.progressbar.visibility = View.INVISIBLE
                    Toast.makeText(myContext, "Error adding document", Toast.LENGTH_SHORT
                    ).show()
                }
        }
    }

    private fun showSnackbarMessage(message: String) {
        Snackbar.make(binding.rvCerpens, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        loadCerpens()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                REQUEST_ADD -> if (resultCode == RESULT_ADD) {
                    loadCerpens()
                    showSnackbarMessage("Satu item berhasil ditambahkan")
                }
                REQUEST_UPDATE ->
                    when (resultCode) {
                        RESULT_UPDATE -> {
                            loadCerpens()
                            showSnackbarMessage("Satu item berhasil diubah")
                        }
                        RESULT_DELETE -> {
                            loadCerpens()
                            showSnackbarMessage("Satu item berhasil dihapus")
                        }
                    }
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RecyclerViewFragment.
         */
//        // TODO: Rename and change types and number of parameters
//        @JvmStatic
//        fun newInstance(param1: String, param2: String) =
//            RecyclerViewFragment().apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
//            }
    }
}