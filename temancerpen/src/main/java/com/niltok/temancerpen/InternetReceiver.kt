package com.niltok.temancerpen

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.niltok.temancerpen.util.NetworkUtil


abstract class InternetReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        var status: String = NetworkUtil.getConnectivityStatusString(context).toString()
        if(status.isEmpty()
            || status.equals("No internet is available")
            || status.equals("No Internet Connection")) {
            status="No Internet Connection"

            onNetworkChange(status)
        }
        Log.d("network", status)
        Toast.makeText(context, status, Toast.LENGTH_LONG)
    }

    protected abstract fun onNetworkChange(status: String)
}