package com.niltok.temancerpen.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.niltok.temancerpen.DetailActivity
import com.niltok.temancerpen.data.Cerpen
import com.niltok.temancerpen.R

class CardViewCerpenDataAdapter(val context: Context):
    RecyclerView.Adapter<CardViewCerpenDataAdapter.CardViewViewHolder>() {

    var listCerpens: ArrayList<Cerpen> = ArrayList<Cerpen>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): CardViewViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_cardview, viewGroup, false)
        return CardViewViewHolder(view)
    }

    override fun onBindViewHolder(holder: CardViewViewHolder, position: Int) {
        holder.bind(listCerpens[position], context)
    }
    override fun getItemCount(): Int = listCerpens.size

    class CardViewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgCover: ImageView
        val tvTitle: TextView
        val tvContent: TextView
        val tvAuthor: TextView
        val tvCategory: TextView

        init {
            imgCover = itemView.findViewById(R.id.img_item_cover)
            tvTitle = itemView.findViewById(R.id.tv_item_title)
            tvContent = itemView.findViewById(R.id.tv_item_content)
            tvAuthor = itemView.findViewById(R.id.tv_item_author)
            tvCategory = itemView.findViewById(R.id.tv_item_category)
        }

        fun bind(cerpenData: Cerpen, context: Context) {
            val cg = ColorGenerator.MATERIAL
            with(itemView){
                Glide.with(itemView.context)
                    .load(cerpenData.cover)
                    .placeholder(
                        TextDrawable
                            .builder()
                            .buildRect(cerpenData.title?.take(2), cg.randomColor)
                    )
                    .apply(RequestOptions().override(350, 550))
                    .into(imgCover)
            }
            tvTitle.text = cerpenData.title
            tvContent.text = cerpenData.content?.take(160)
            tvAuthor.text = cerpenData.author
            tvCategory.text = cerpenData.category
            itemView.setOnClickListener {
                Toast.makeText(itemView.context, "Kamu memilih " + cerpenData.title,
                    Toast.LENGTH_SHORT).show()
                val moveWithObjectIntent = Intent(context, DetailActivity::class.java)
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MYDATA, cerpenData)
                context.startActivity(moveWithObjectIntent)
            }
        }
    }
}