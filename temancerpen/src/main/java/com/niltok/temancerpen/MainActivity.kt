package com.niltok.temancerpen

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.google.android.material.snackbar.Snackbar
import com.niltok.temancerpen.ui.AboutFragment
import com.niltok.temancerpen.ui.LikeFragment
import com.niltok.temancerpen.ui.RecyclerViewFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Handle the splash screen transition.
        installSplashScreen().apply {
        }
        setContentView(R.layout.activity_main)

        var internetReceiver = object : InternetReceiver() {
            override fun onNetworkChange(status: String) {
                Snackbar.make(this@MainActivity, findViewById(R.id.fragment_container_view), status, Snackbar.LENGTH_LONG)
            }
        }

        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .replace(R.id.fragment_container_view, RecyclerViewFragment::class.java, null)
            .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        setMode(item.itemId)
        return super.onOptionsItemSelected(item)
    }
    private fun setMode(selectedMode: Int) {
        when (selectedMode) {
            R.id.action_home -> {
                supportFragmentManager.beginTransaction()
                    .setReorderingAllowed(true)
                    .replace(R.id.fragment_container_view, RecyclerViewFragment::class.java, null)
                    .commit()
            }
            R.id.action_like -> {
                supportFragmentManager.beginTransaction()
                    .setReorderingAllowed(true)
                    .replace(R.id.fragment_container_view, LikeFragment::class.java, null)
                    .commit()
            }
            R.id.action_account -> {
                val intent = Intent(this@MainActivity, AccountActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.action_about -> {
                supportFragmentManager.beginTransaction()
                    .setReorderingAllowed(true)
                    .replace(R.id.fragment_container_view, AboutFragment::class.java, null)
                    .commit()
            }
        }
    }
}